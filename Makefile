# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tkekae <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/20 08:38:46 by tkekae            #+#    #+#              #
#    Updated: 2018/09/08 16:26:09 by tkekae           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = tkekae.filler

CC = gcc

CFLAGS = -Wall -Werror -Wextra

SRC_DIR = ./srcs/

INC_DIR = -I include/

SRCS =	*.c

SRC = $(addprefix $(SRC_DIR), $(SRCS))

OBJ = $(SRCS:.c=.o)

$(NAME):
	make re -C libft/
	$(CC) $(INC_DIR) -c $(SRC)
	$(CC) -o $(NAME) $(OBJ) -L libft/ -lft
	make clean

all : $(NAME)

.PHONY: clean fclean re

clean:
	rm -f $(OBJ)
	make -C libft/ clean

fclean:
	rm -f $(OBJ)
	rm -f $(NAME)
	make -C libft/ fclean

re: fclean all

