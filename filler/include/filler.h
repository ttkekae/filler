/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 08:35:23 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 16:45:40 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "../libft/libft.h"
typedef	struct	s_list
{
	int y;
	int x;
	struct	s_list *next;
}				t_list;
typedef	struct	s_node
{
	int x;
	int y;
}				t_node;
typedef	struct	s_info
{
	char	me;
	char	foe;
	int		map_y;
	int		map_x;
	int		piece_y;
	int		piece_x;
}				t_info;
typedef struct s_filler
{
	t_info	info;
	t_node	trimed;
	t_node	top_right;
	t_node	bottom_left;
	int	game_status;
	char	**map;
	char	**piece;
	char 	**trimed_p;
}				t_filler;

int		ft_check_move(t_filler *filler, int y, int x);
int		ft_best_moves(t_filler *f, t_list *head);
int		ft_dst(t_filler *f, int y, int x);
int		ft_list_moves(t_filler *filler);
t_filler	*ft_init_filler(void);
void	ft_get_info(t_filler *filler);
void	ft_init_list(t_list *list);
void	ft_piece_info(t_filler *filler);
void	ft_init_info(t_filler *filler);
void	ft_map_info(t_filler *filler);
void	ft_player(t_filler *filler);
void	ft_trim_piece(t_filler *filler);
void	ft_trim(t_filler *filler);
#endif
