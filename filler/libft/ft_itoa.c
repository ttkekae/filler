/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 13:42:46 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/12 16:03:20 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#	define MIN_SIZE	12

static	int		ft_sizealloc(int n)
{
	int count;

	count = 0;
	while (n > 9)
	{
		n /= 10;
		count++;
	}
	count++;
	return (count);
}

static	char	*ft_minvalue(void)
{
	char	*fresh;

	fresh = ft_strnew(MIN_SIZE);
	ft_strcpy(fresh, "-2147483648");
	return (fresh);
}

char			*ft_itoa(int n)
{
	int		table[2];
	char	*fresh;

	table[0] = n;
	if (n == -2147483648)
		return (ft_minvalue());
	if (table[0] < 0)
	{
		table[0] *= -1;
		table[1] = ft_sizealloc(table[0]) + 1;
	}
	else
		table[1] = ft_sizealloc(table[0]);
	if (!(fresh = ft_strnew(table[1])))
		return (NULL);
	fresh[table[1]--] = '\0';
	while (table[0] > 9)
	{
		fresh[table[1]--] = table[0] % 10 + 48;
		table[0] /= 10;
	}
	fresh[table[1]--] = table[0] % 10 + 48;
	if (n < 0)
		fresh[table[1]] = '-';
	return (fresh);
}
