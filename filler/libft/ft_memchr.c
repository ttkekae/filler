/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 17:06:53 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 16:49:01 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *src, int c, size_t len)
{
	size_t			i;
	unsigned char	*newsrc;
	unsigned char	newchar;

	i = 0;
	newchar = (unsigned char)c;
	newsrc = (unsigned char*)src;
	while (len > 0)
	{
		if (newsrc[i] == newchar)
		{
			return (newsrc + i);
		}
		else
			i++;
		len--;
	}
	return (NULL);
}
