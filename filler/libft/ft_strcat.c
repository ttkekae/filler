/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 16:12:49 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/07 12:34:46 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dest, const char *source)
{
	char	*temp;
	int		i;
	int		j;

	i = 0;
	temp = NULL;
	temp = dest;
	i = ft_strlen(dest);
	j = 0;
	while (source[j])
	{
		temp[i] = source[j];
		i++;
		j++;
	}
	temp[i] = '\0';
	return (dest);
}
