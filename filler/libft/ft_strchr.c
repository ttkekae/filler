/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 16:01:04 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 17:06:38 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *str, int c)
{
	unsigned char	new_c;
	int				i;
	int				len;

	i = 0;
	new_c = (unsigned char)c;
	if (c == '\0')
	{
		len = ft_strlen(str);
		return ((char *)str + len);
	}
	while (str[i] != '\0')
	{
		if (str[i] == new_c)
			return ((char *)str + i);
		i++;
	}
	return (NULL);
}
