/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 09:49:27 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/01 17:59:56 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	size_t	size;
	char	*temp;

	size = ft_strlen(str);
	temp = ft_memalloc(size + 1);
	if (temp == NULL)
		return (NULL);
	ft_strcpy(temp, str);
	return (temp);
}
