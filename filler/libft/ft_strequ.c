/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 11:13:24 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/01 18:01:50 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *str1, char const *str2)
{
	if (str1 == NULL || str2 == NULL)
		return (0);
	if (ft_strcmp(str1, str2) == 0)
		return (1);
	else
		return (0);
}
