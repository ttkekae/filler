/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 11:54:58 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/01 14:26:40 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *str1, char const *str2)
{
	char	*fresh;

	if (str1 == NULL || str2 == NULL)
		return (NULL);
	fresh = ft_memalloc(ft_strlen(str1) + ft_strlen(str2) + 1);
	if (fresh == NULL)
		return (NULL);
	ft_strcpy(fresh, str1);
	ft_strcat(fresh, str2);
	return (fresh);
}
