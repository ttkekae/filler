/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/31 14:55:48 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 17:19:35 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *str, char (*f)(char))
{
	char	*fresh;
	int		i;
	int		size;

	if (str == NULL)
		return (NULL);
	i = 0;
	fresh = NULL;
	size = ft_strlen(str);
	fresh = ft_memalloc(size + 1);
	if (fresh == NULL)
		return (NULL);
	while (str[i] != 0)
	{
		fresh[i] = f(str[i]);
		i++;
	}
	fresh[i] = '\0';
	return (fresh);
}
