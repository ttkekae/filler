/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 16:50:34 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/04 10:41:40 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *source, size_t num)
{
	char	*temp;
	int		i;
	size_t	j;

	temp = dest;
	i = ft_strlen(dest);
	j = 0;
	while (j < num && source[j])
	{
		temp[i] = source[j];
		j++;
		i++;
	}
	temp[i] = '\0';
	return (dest);
}
