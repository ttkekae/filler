/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 11:34:41 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/01 18:02:49 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strnequ(char const *str1, char const *str2, size_t n)
{
	if (str1 == NULL || str2 == NULL)
		return (0);
	if (ft_strncmp(str1, str2, n) == 0)
		return (1);
	else
		return (0);
}
