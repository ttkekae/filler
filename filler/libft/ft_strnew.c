/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 10:13:05 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 17:36:13 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*fresh;

	fresh = ft_memalloc(size + 1);
	if (fresh == NULL)
		return (NULL);
	ft_memset(fresh, '\0', size + 1);
	return (fresh);
}
