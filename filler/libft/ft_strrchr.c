/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 16:26:54 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 17:41:06 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	unsigned char	new_c;
	int				len;

	new_c = (unsigned char)c;
	len = ft_strlen(str);
	if (c == '\0')
		return ((char *)str + len);
	while (len >= 0)
	{
		if (str[len] == new_c)
			return ((char *)str + len);
		len--;
	}
	return (NULL);
}
