/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_move.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 08:54:53 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 17:13:32 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

static	int	ft_check_me(t_filler *filler, int y, int x)
{
	if (ft_tolower(filler->map[y][x]) == filler->info.me)
		return (1);
	else
		return (0);
}
static	int	ft_check_foe(t_filler *filler, int y, int x)
{
	if (ft_tolower(filler->map[y][x]) == filler->info.foe)
		return (1);
	return (0);
}
int	ft_check_move(t_filler *f, int y, int x)
{
	int		overlap;
	int		i;
	int		j;

	overlap = 0;
	i = 0;
	while (i < f->trimed.y)
	{
		j = 0;
		while (j < f->trimed.x)
		{
			if (f->trimed_p[i][j] == '*')
			{
			   	overlap += (ft_check_me(f, i + x, j + y)) ? 1 : 0;
				if (ft_check_foe(f, i + x, j + y))
					return (0);
			}
			j++;
			if (overlap > 1)
				return (0);
		}
		i++;
	}
	return (overlap == 1? 1 : 0);
}
/*int	ft_best_moves(t_filler *f, t_list *head)
{
	int 	max;
	int	pos[2];

	max = 2147483647;
	if (head == NULL)
		return (0);
	else
	{
		while(head)
		{
			if (ft_dst(f, head->y, head->x) < max)
			{
				max = ft_dst(f, head->y, head->x);
				pos[0] = head->y;
				pos[1] = head->x;
			}
			head = head->next;
		}
	}
	ft_putnbr(pos[0] - f->top_right.y);
	ft_putchar(' ');
	ft_putnbr(pos[1] - f->top_right.x);
	ft_putchar('\n');
	return (1);
}*/
