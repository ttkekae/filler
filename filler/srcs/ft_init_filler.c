/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_filler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/17 08:28:42 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/21 07:24:04 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

void	ft_init_list(t_list *list)
{
	list = (t_list *)malloc(sizeof(t_list));
	list->x = 0;
	list->y = 0;
	list->next = NULL;
}
t_filler	*ft_init_filler()
{
	t_filler *filler;

	filler = (t_filler *)ft_memalloc(sizeof(t_filler));
	filler->trimed.y = 0;
	filler->trimed.x = 0;
	filler->top_right.y = 0;
	filler->top_right.x = 0;
	filler->bottom_left.y = 0;
	filler->bottom_left.x = 0;
	filler->map = NULL;
	filler->piece = NULL;
	filler->trimed_p = NULL;
	filler->info.map_y = 0;
	filler->info.map_x = 0;
	filler->info.piece_y = 0;
	filler->info.piece_x = 0;
	return (filler);
}
