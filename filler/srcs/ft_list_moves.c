/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_moves.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 16:47:09 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 16:38:41 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"
#include <stdio.h>

void  print_list(t_list *head)
{
	if (head != NULL)
	{
		while(head->next)
		{
			printf("y := %i  ,",head->y);
			printf("x := %i\n",head->x);
			head = head->next;
		}
	}
	else
	    printf("no lst");
}

t_list	*ft_node(int y, int x)
{
	t_list *ptr;

	if (!(ptr = (t_list *)ft_memalloc(sizeof(t_list))))
		return (0);
	else
	{
		ptr->y = y;
		ptr->x = x;
		ptr->next = NULL;
	}
	return (ptr);
}
void	ft_add_to_list(t_list **head, t_list *node)
{
	t_list *current;

	current = *head;
	if (*head == NULL)
		*head = node;
	else
	{
		while (current->next)
			current = current->next;
		current->next = node;
	}
}
int	ft_list_moves(t_filler *filler)
{
	int		i;
	int		j;
	int		count;
	t_list		*head;

	count = 0;
	i = 0;
	head = NULL;
	while (i < (filler->info.map_y - filler->trimed.y))
	{
		j = 0;
		while (j < (filler->info.map_x - filler->trimed.x))
		{
			if (ft_check_move(filler, i, j) == 1)
			{
				ft_add_to_list(&head, ft_node(i, j));
				count++;
			}
			j++;
		}
		i++;
	}
	print_list(head);
/*	if (ft_best_moves(filler, head))
		return (1);*/
	return (0);
}
