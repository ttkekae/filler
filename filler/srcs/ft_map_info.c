/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_info.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/17 09:03:56 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 16:53:21 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

void	ft_map_info(t_filler *filler)
{
	int		i;
	char	*line;

	i = 0;
	filler->map = (char **)malloc(sizeof(char*) * filler->info.map_y);
	if (filler->map == NULL)
		return ;
	while (i < filler->info.map_y)
	{
		get_next_line(0, &line);
		filler->map[i]= ft_strdup(line + 4);
		i++;
		ft_strdel(&line);
	}
}
