/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_piece_info.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 15:04:31 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 16:59:31 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

void	ft_piece_info(t_filler *filler)
{
	int		i;
	char	*line;

	i = 0;
	filler->piece = (char **)malloc(sizeof(char*) * filler->info.piece_x);
	if (filler->piece == NULL)
		return ;
	while (i < filler->info.piece_x)
	{
		get_next_line(0, &line);
		filler->piece[i] = ft_strdup(line);
		i++;
	}
}
