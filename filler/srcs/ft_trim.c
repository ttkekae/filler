/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 10:01:20 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 15:46:19 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

void	ft_trim(t_filler *filler)
{
	int		i[2];
	int		y;
	int		x;
	char	*str;
	int		index;

	i[0] = filler->info.piece_y - filler->top_right.y - filler->bottom_left.y;
	filler->trimed_p = (char **)malloc(sizeof(char *) * i[0]);
	i[0] = filler->top_right.y;
	y = filler->info.piece_y - filler->bottom_left.y;
	x = filler->info.piece_x - filler->bottom_left.x - filler->top_right.x;
	index = 0;
	while (i[0] < y)
	{
		str = filler->piece[i[0]];
		filler->trimed_p[index] = ft_strsub(str, filler->top_right.x, x);
		i[0]++;
		index++;
	}
	i[1] = 0;
	while (filler->trimed_p[0][i[1]])
		i[1]++;
	filler->trimed.y = index;
	filler->trimed.x = i[1];
}
