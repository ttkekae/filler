/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim_piece.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 15:12:08 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 15:15:23 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

int	ft_trim_left(t_filler *filler)
{
	int	i;
	int j;

	j = 0;
	while (j < filler->info.piece_x)
	{
		i = 0;
		while (i < filler->info.piece_y)
		{
			if (filler->piece[i][j] == '*')
				return (j);
			i++;
		}
		j++;
	}
	return (0);
}
int	ft_trim_right(t_filler *filler)
{
	int	i;
	int j;
	int count;

	count = -1;
	j = filler->info.piece_x - 1;
	while (j != -1)
	{
		count++;
		i = 0 ;
		while (i < filler->info.piece_y)
		{
			if (filler->piece[i][j] == '*')
				return (count);
			i++;
		}
		j--;
	}
	return (0);
}
int	ft_trim_up(t_filler *filler)
{
	int	i;
	int j;

	i = 0;
	while (i < filler->info.piece_x)
	{
		j = 0;
		while (j < filler->info.piece_y)
		{
			if (filler->piece[i][j] == '*')
				return (i);
			j++;
		}
		i++;
	}
	return (0);
}
int	ft_trim_down(t_filler *filler)
{
	int	i;
	int j;
	int count;

	count = -1;
	i = filler->info.piece_y - 1;
	while (i !=  -1)
	{
		j = 0;
		count++;
		while (j < filler->info.piece_x)
		{
			if (filler->piece[i][j] == '*')
				return (count);
			j++;
		}
		i--;
	}
	return (0);
}
void	ft_trim_piece(t_filler *filler)
{
	filler->top_right.y = ft_trim_up(filler);
	filler->top_right.x = ft_trim_left(filler);
	filler->bottom_left.y = ft_trim_down(filler);
	filler->bottom_left.x = ft_trim_right(filler);
}
