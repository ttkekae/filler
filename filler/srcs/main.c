/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 15:58:17 by tkekae            #+#    #+#             */
/*   Updated: 2018/09/08 16:06:20 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

int	main()
{
	t_filler	*filler;

	filler = ft_init_filler();
	ft_player(filler);
	/*while (1)
	{*/
		ft_get_info(filler);
		ft_trim_piece(filler);
		ft_trim(filler);
		ft_list_moves(filler);
//		print_list();
/*
		if (!ft_list_moves(filler))
		{
			ft_putendl("0 0");
			return (0);
		}
	}}*/
	return (0);
}
