/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 08:35:23 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/27 13:30:33 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "../../libft/libft/libft.h"
typedef	struct	s_list
{
	int x;
	int y;
	struct	s_list *next;
}				t_list;
typedef	struct	s_node
{
	int x;
	int y;
}				t_node;
typedef	struct	s_info
{
	char	me;
	char	foe;
	int		map_x;
	int		map_y;
	int		piece_x;
	int		piece_y;
}				t_info;
typedef struct s_filler
{
	t_node	trimed;
	t_info	info;
	t_node	top_right;
	t_node	bottom_left;
	char	**map;
	char	**piece;
	char 	**trimed_p;
}				t_filler;

int		ft_check_move(t_filler *filler,int x,int y);
//t_node	ft_coordinate(t_info *info, int x, int y);
void	ft_list_moves(t_filler *filler, t_list **start);
void	ft_get_info(t_filler *filler);
void	ft_init_list(t_list *list);
void	ft_piece_info(t_filler *filler);
void	ft_init_info(t_info *info);
void	ft_init_node(t_node *node);
void	ft_map_info(t_filler *filler);
void	ft_player(t_filler *filler);
void	ft_trim_piece(t_filler *filler);
void	ft_init_filler(t_filler *filler);
//t_node	**ft_valid_moves(t_info	*info);
//void	ft_init_filler(t_filler *filler);
void	ft_trim(t_filler *filler);
#endif
