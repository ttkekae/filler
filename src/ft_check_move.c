/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_move.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 08:54:53 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/27 13:30:49 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int	ft_check(t_filler *fill, char c)
{
	if (c == ft_tolower(fill->info.me) || c == ft_toupper(fill->info.me))
		return (1);
	else
		return (0);
}

int	ft_check_move(t_filler *filler, int x, int y)
{
	int		overlap;
	int		i;
	int		j;

	overlap = 0;
	i = 0;
	while (i < filler->trimed.x)
	{
		j = 0;
		while (j < filler->trimed.y)
		{
			if (filler->trimed_p[i][j] == '*')
			{
				if (ft_check(filler, filler->map[i + x][j + y]) == 1)
					overlap++;
				else
					return (0);
			}
			j++;
		}
		i++;
	}
	if (overlap == 1)
		return (overlap);
	else
		return (0);
}
