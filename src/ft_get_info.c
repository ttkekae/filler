/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_info.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 10:49:26 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/24 16:13:07 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_piece(t_filler *filler)
{
	int		i;
	int		j;
	char	*fresh;
	char	*temp;

	i = 0;
	while ((i = get_next_line(0,&fresh)) > 0)
	{
		if ((temp = ft_strstr(fresh, "Piece")) != NULL)
		{
			j = 6;
			filler->info.piece_x = ft_atoi(&fresh[j]);
			while (fresh[j] != ' ')
				j++;
			filler->info.piece_y = ft_atoi(&fresh[++j]);
			break;
		}
	}
}
void	ft_map(t_filler *filler)
{
	int		i;
	int		j;
	char	*fresh;
	char	*temp;

	i = 0;
	while ((i = get_next_line(0,&fresh)) > 0)
	{
		if ((temp = ft_strstr(fresh, "Plateau")) != NULL)
		{
			j = 8;
			filler->info.map_x = ft_atoi(&fresh[j]);
			while (fresh[j] != ' ')
				j++;
			filler->info.map_y = ft_atoi(&fresh[++j]);
			get_next_line(0,&fresh); 
			break ;
		}
	}
	ft_map_info(filler);
	ft_piece(filler);
	ft_piece_info(filler);
}
/*void	ft_info(t_filler *filler)
{
	int		i;
	char	*line;
	char	*temp;

	i = 0;
	while ((i = get_next_line(0, &line)) > 0)
	{
		if ((temp = ft_strstr(line, "p1")) != NULL)
		{
			filler->info.my_player = 'o';
			filler->info.foe = 'x';
		}
		else
		{
			filler->info.my_player = 'x';
			filler->info.foe = 'o';
		}
		break ;
		}
}*/
void	ft_get_info(t_filler *filler)
{
//	ft_info(filler);
	ft_map(filler);
}
