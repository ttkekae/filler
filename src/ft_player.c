/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_player.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 16:11:29 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/27 13:07:14 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_player(t_filler *filler)
{
	int		i;
	char	*line;
	char	*temp;

	i = 0;
	while ((i = get_next_line(0, &line)) > 0)
	{
		if ((temp = ft_strstr(line, "p1")) != NULL)
		{
			filler->info.me = 'o';
			filler->info.foe = 'x';
		}
		else
		{
			filler->info.me = 'x';
			filler->info.foe = 'o';
		}
		break ;
		}
	ft_strdel(&line);
}
