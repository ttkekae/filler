/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/20 10:01:20 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/23 14:08:32 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <stdio.h>
void	ft_trim(t_filler *filler)
{
	int		i[2];
	int		x;
	int		y;
	char	*str;
	int		index;

	i[0] = filler->info.piece_x - filler->top_right.x - filler->bottom_left.x;
	filler->trimed_p = (char **)malloc(sizeof(char *) * i[0]);
	i[0] = filler->top_right.x;
	x = filler->info.piece_x - filler->bottom_left.x;
	y = filler->info.piece_y - filler->bottom_left.y - filler->top_right.y;
	index = 0;
	while (i[0] < x)
	{
		str = filler->piece[i[0]];
		filler->trimed_p[index] = ft_strsub(str, filler->top_right.y, y);
		i[0]++;
		index++;
	}
	i[1] = 0;
	while (filler->trimed_p[0][i[1]])
		i[1]++;
	filler->trimed.x = index;
	filler->trimed.y = i[1];
	printf("timed \n x :=%i\n y :=%i\n",filler->trimed.x, filler->trimed.y);
}
