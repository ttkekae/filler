/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 15:58:17 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/27 12:24:14 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <fcntl.h>
#include <stdio.h>

int	main()
{
	t_filler	filler;
	t_list		*start;
	ft_init_filler(&filler);
	ft_player(&filler);
	while (1)
	{
		ft_get_info(&filler);
		ft_trim_piece(&filler);
		ft_trim(&filler);
		ft_list_moves(&filler, &start);
	}
	return (0);
	/*
printf("timed  2\n x :=%i\n y :=%i\n",filler.trimed.x, filler.trimed.y);
	int i;
	i = 0;
	printf("map\n =======================\n");
	printf("x :=%i\n",filler.info.map_x);
	printf("y :=%i\n",filler.info.map_y);
	printf("piece\n =======================\n");
	printf("x :=%i\n",filler.info.piece_x);
	printf("y :=%i\n",filler.info.piece_y);
	printf("player info\n =======================\n");
	printf("me :=%c\n",filler.info.my_player);
	printf("foe :=%c\n",filler.info.foe);
	printf("trime\n =======================\n");
	printf("top.x :=%i\n",filler.top_right.x);
	printf("top.y :=%i\n",filler.top_right.y);
	printf("bottom.x :=%i\n",filler.bottom_left.x);
	printf("bottom.y:= %i\n",filler.bottom_left.y);
	printf("MAP\n =======================\n");
	while (i < filler.info.map_x)
	{
		printf("%s\n",filler.map[i]);
		i++;
	}
	i = 0;
	int j = 0;
	printf("piece\n =======================\n");
	while (i < filler.info.piece_x)
	{
		printf("%s\n",filler.piece[i]);
		i++;
	}
/	ft_trim(&filler);
	i = 0;
	printf(" trimed : x:= %i\n y := %i\n",filler.trimed.x,filler.trimed.y);
	while (i < filler.trimed.x)
	{
		printf("%s\n",filler.trimed_p[i]);
		i++;
	}*/
	return (0);
}
