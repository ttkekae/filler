/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_move.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 08:54:53 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/28 16:37:32 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <stdio.h>

int	ft_check(t_filler *f, char c)
{
	if (c == ft_tolower(f->info.me) /*|| c == ft_toupper(f->info.me)*/)
		return (1);
	else
		return (0);
}
int	ft_check_move(t_filler *f, int x, int y)
{
	int		overlap;
	int		i;
	int		j;
	char	who;

	overlap = 0;
	i = 0;
	while (i < f->trimed.x)
	{
		j = 0;
		while (j < f->trimed.y)
		{
			who = ft_tolower(f->map[i + x][j + j]);
			if (f->trimed_p[i][j] == '*' && ft_check(f, ft_tolower(f->map[i + x][j + y])))
			{
				if (ft_check(f, ft_tolower(f->map[i + x][j + y])) == 1)
					overlap++;
			}
			j++;
		}
		i++;
	}
	if (overlap == 1)
		return (overlap);
	else
		return (0);
}
