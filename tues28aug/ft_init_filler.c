/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_filler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/17 08:28:42 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/28 14:06:41 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_init_filler(t_filler *filler)
{
	ft_init_info(&filler->info);
	ft_init_node(&filler->trimed);
	ft_init_node(&filler->top_right);
	ft_init_node(&filler->bottom_left);
	filler->count = 0;
	filler->map = NULL;
	filler->piece = NULL;
	filler->trimed_p = NULL;
}
