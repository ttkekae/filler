/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_moves.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 16:47:09 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/28 14:22:11 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <stdio.h>

t_list	*ft_node(int x, int y)
{
	t_list *ptr;

	if (!(ptr = (t_list *)ft_memalloc(sizeof(t_list))))
		return (0);
	else
	{
		ptr->x = x;
		ptr->y = y;
		ptr->next = NULL;
	}
	return (ptr);
}
void	ft_add_to_list(t_list **head, t_list *node)
{
	t_list *current;

	current = *head;
	if (head == NULL)
		*head = node;
	else
	{
		while (current->next)
			current = current->next;
		current->next = node;
	}
}
void	ft_list_moves(t_filler *filler, t_list **start)
{
	int i;
	int j;
	int count;

	count = 0;
	i = 0;
	while (i < (filler->info.map_x - filler->trimed.x))
	{
		j = 0;
		while (j < (filler->info.map_y - filler->trimed.y))
		{
			if (ft_check_move(filler, i, j) == 1)
			{
				ft_add_to_list(start, ft_node(i, j));
				count++;
				printf("valid \n:-----------------:\n");
				printf("%i %i"/*,start->x, start->y*/, i, j);
			}
			j++;
		}
		i++;
	}
	filler->count = count;
	/*if (count == 0)
		printf("%i %i", 0, 0);*/
}
