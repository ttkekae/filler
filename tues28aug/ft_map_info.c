/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_info.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/17 09:03:56 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/17 11:37:52 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_map_info(t_filler *filler)
{
	int		i;
	char	*line;

	i = 0;
	filler->map = (char **)malloc(sizeof(char*) * filler->info.map_x);
	if (filler->map == NULL)
		return ;
	while (i < filler->info.map_x)
	{
		get_next_line(0, &line);
		filler->map[i]= ft_strdup(line + 4);
		i++;
	}
}
