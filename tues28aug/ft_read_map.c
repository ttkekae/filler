/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 15:26:02 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/28 14:34:31 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <fcntl.h>
#include <stdio.h>

int	main()
{
	t_filler	filler;
	t_list		*start;

	ft_init_filler(&filler);
	ft_player(&filler);

	printf("\t INFO\n :-----------------------------------------------------:\n");
	printf("me := %i\n",filler.info.me);
	printf("foe :=%i\n",filler.info.foe);
	while (1)
	{

	ft_get_info(&filler);
	ft_trim_piece(&filler);
	int i;
	i = 0;
	printf("\t\t MAP\n :-----------------------------------------------------:\n");
	printf("map x:= %i\n",filler.info.map_x);
	printf("map y:= %i\n",filler.info.map_y);
	printf("piece x:= %i\n",filler.info.piece_x);
	printf("piece y:= %i\n",filler.info.piece_y);
	
	while (i < filler.info.map_x)
	{
		printf("%s\n",filler.map[i]);
		i++;
	}
	i = 0;
	int j = 0;
	printf("\t\t PIECE\n :-----------------------------------------------------:\n");
	while (i < filler.info.piece_x)
	{
		printf("%s\n",filler.piece[i]);
		i++;
	}
	ft_trim(&filler);
	i = 0;
	printf("\t\t TRIMED\n :-----------------------------------------------------:\n");
	while (i < filler.trimed.x)
	{
		printf("%s\n",filler.trimed_p[i]);
		i++;
	}
	ft_list_moves(&filler, &start);
	if (filler.count == 0)
	{
		printf("%i %i", 0, 0);
		return (0);
	}
	}
	return (0);
}
