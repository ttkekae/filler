/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/12 17:38:31 by tkekae            #+#    #+#             */
/*   Updated: 2018/08/15 16:31:56 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# define BUFF_SIZE 1
# include <unistd.h>
# include <string.h>
# include <stdlib.h>

typedef	struct	s_node
{
	int point_x;
	int point_y;
	int	avail;
}				t_node;
typedef	struct	s_info
{
	int	map_x;
	int	map_y;
	int	piece_x;
	int	piece_y;
	t_node	top_right;
	t_node	bottom_left;
	t_node	**map;
	t_node	**piece;
}				t_info;
int		ft_atoi(const char *str);
void	ft_bzero(void *str, size_t n);
int		ft_isascii(int c);
int		ft_isprint(int c);
void	*ft_memccpy(void *dest, const void *source, int c, size_t n);
void	*ft_memchr(const void *src, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);
void	*ft_memmove(void *dst, const void *src, size_t len);
void	*ft_memset(void *ptr, int c, size_t len);
char	*ft_strcat(char *dest, const char *source);
int		ft_strcmp(const char *str1, const	char *str2);
char	*ft_strdup(const char	*str);
size_t	ft_strlen(const	char *str);
size_t	ft_strlcat(char *dst, const char *src, size_t size);
int		ft_memcmp(const void *s1, const void *s2, size_t n);
char	*ft_strnstr(const char *haystack, const char *needle, size_t len);
char	*ft_strncat(char *dest, const char *source, size_t num);
char	*ft_strrchr(const char *s, int c);
int		ft_strncmp(const char *str1, const char *str2, size_t num);
int		ft_tolower(int c);
int		ft_toupper(int c);
int		ft_strchri(char *str, int c);
char	*ft_strchr(const char *str, int c);
char	*ft_strstr(const char *haystack, const char *needle);
char	*ft_strcpy(char *dst, const char *src);
char	*ft_strncpy(char *dst, const char *src, size_t len);
int		ft_isalpha(int c);
int		ft_isdigit(int c);
int		ft_isalnum(int c);
void	*ft_memalloc(size_t size);
void	ft_memdel(void **ap);
char	*ft_strnew(size_t size);
void	ft_strdel(char **as);
void	ft_strclr(char *s);
void	ft_striter(char *s, void (*f)(char *));
void	ft_striteri(char *s, void (*f)(unsigned int, char *));
char	*ft_strmap(char const *s, char (*f)(char));
char	*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int		ft_strequ(char const *s1, char const *s2);
int		ft_strnequ(char const *s1, char const *s2, size_t n);
char	*ft_strsub(char const *s, unsigned int start, size_t len);
char	*ft_strjoin(char const *s1, char const *s2);
char	*ft_strtrim(char const *s);
char	**ft_strsplit(char const *s, char c);
char	*ft_itoa(int n);
void	ft_putchar(char c);
void	ft_putstr(char const *s);
void	ft_putendl(char const *s);
void	ft_putnbr(int n);
void	ft_putchar_fd(char c, int fd);
void	ft_putstr_fd(char const *s, int fd);
void	ft_putendl_fd(char const *s, int fd);
void	ft_putnbr_fd(int n, int fd);
int		get_next_line(int const fd, char **line);
#endif
